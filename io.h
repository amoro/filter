#ifndef ____IO____
#define ____IO____

#include <memory>

namespace detail {

// wrappers for io streams -
// could hold reference or stream object itself
// user can get a reference out of it

template <class T>
class Wrapper
{
public:
    virtual ~Wrapper() {}
    virtual T& unwrap() = 0;
};

// reference wrapper
template <class T>
class RefWrapper
    : public Wrapper<T>
{
private:
    T &ref_;
public:
    RefWrapper(T &ref): ref_(ref) {}
    T& unwrap() override { return ref_; }
};

// value wrapper
template <class T>
class HolderWrapper
    : public Wrapper<T>
{
private:
    std::unique_ptr<T> obj_;
public:
    HolderWrapper(std::unique_ptr<T> &&obj): obj_(std::move(obj)) {}
    T& unwrap() override { return *obj_; }
};

}

#include <iostream>
#include <fstream>

using InputStream = detail::Wrapper<std::istream>;
using OutputStream = detail::Wrapper<std::ostream>;

// wrap std::cin
inline std::unique_ptr<InputStream> wrap_cin()
{
    using namespace detail;
    return std::unique_ptr<InputStream>(new RefWrapper<std::istream>(std::cin));
}

// wrap input file stream object
inline std::unique_ptr<detail::Wrapper<std::istream>> wrap_ifstream(std::unique_ptr<std::ifstream> &&f)
{
    using namespace detail;
    return std::unique_ptr<InputStream>(new HolderWrapper<std::istream>(std::move(f)));
}

// wrap std::cout
inline std::unique_ptr<OutputStream> wrap_cout()
{
    using namespace detail;
    return std::unique_ptr<OutputStream>(new RefWrapper<std::ostream>(std::cout));
}

// wrap output file stream object
inline std::unique_ptr<OutputStream> wrap_ofstream(std::unique_ptr<std::ofstream> &&f)
{
    using namespace detail;
    return std::unique_ptr<OutputStream>(new HolderWrapper<std::ostream>(std::move(f)));
}

#endif


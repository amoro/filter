#include "record.h"

#include <algorithm>

Either<std::string, Record> parse_record(const std::string &s, const std::string &sep)
{
    Record rec;
    const auto sep_start = sep.begin(), sep_end = sep.end();
    const auto end = s.end();
    auto start = s.begin();
    auto next = std::search(start, end, sep_start, sep_end);
    if (end == next)
        return make_left<std::string, Record>("Bad record: " + s);
    rec.brand.insert(rec.brand.end(), start, next);
    start = next + 1;
    next = std::search(start, end, sep_start, sep_end);
    if (end == next)
        return make_left<std::string, Record>("Bad record: " + s);
    rec.model.insert(rec.model.end(), start, next);
    start = next + 1;
    next = std::search(start, end, sep_start, sep_end);
    if (end == next)
        return make_left<std::string, Record>("Bad record: " + s);
    if (start != next)
        rec.year = std::stoi(std::string(start, next));
    else
        rec.year = 0;
    start = next + 1;
    if (end == start)
        rec.price = 0.0;
    else
        rec.price = std::stod(std::string(start, end));
    return make_right<std::string, Record>(std::move(rec));
}


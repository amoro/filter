#include "filter.h"

#include <boost/variant.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

namespace filter {

// static filters types list definition
template <FilterType... AllowedTypes>
constexpr std::array<FilterType, sizeof...(AllowedTypes)> ThisOps<AllowedTypes...>::allowed_types_;

}

using filter::FilterType;


// filters specifications

struct BrandFilter
{
    std::string value;
};

struct YearFilter
{
    FilterType type;
    unsigned value;
};

struct PriceFilter
{
    FilterType type;
    double value;
};

typedef boost::variant<BrandFilter, YearFilter, PriceFilter> FieldFilter;

BOOST_FUSION_ADAPT_STRUCT(
    BrandFilter,
    (std::string, value)
)

BOOST_FUSION_ADAPT_STRUCT(
    YearFilter,
    (FilterType, type)
    (unsigned, value)
)

BOOST_FUSION_ADAPT_STRUCT(
    PriceFilter,
    (FilterType, type)
    (double, value)
)

typedef std::string::const_iterator StringIterator;

using namespace boost::spirit;

template <class T> using rule = qi::rule<StringIterator, T>;

// filters grammar:
// one string value contains one filter spec,
// it could be by brand (only equality), by year or by price;
// types for year and price are checked after parsing
struct filter_grammar : qi::grammar<StringIterator, FieldFilter()>
{
    filter_grammar(): filter_grammar::base_type{filter} {
        type = ("==" >> attr(FilterType::eq)) |
            ("!=" >> attr(FilterType::ne)) |
            ("<=" >> attr(FilterType::lte)) |
            (">=" >> attr(FilterType::gte)) |
            ("<" >> attr(FilterType::lt)) |
            (">" >> attr(FilterType::gt));
        brand = "brand==" >> +qi::char_;
        year = "year" >> type >> qi::uint_;
        price = "price" >> type >> qi::double_;
        filter = brand | year | price;
    }
    rule<FilterType()> type;
    rule<BrandFilter()> brand;
    rule<YearFilter()> year;
    rule<PriceFilter()> price;
    rule<FieldFilter()> filter;
};

bool add_filter_spec(const std::string &s, RecordFilter &filter)
{
    // choose one of three filterable fields
    struct FilterMaker: public boost::static_visitor<>
    {
        RecordFilter &filter;
        FilterMaker(RecordFilter &filter_): filter(filter_) {}
        void operator() (const BrandFilter &b) { filter.add_brand(b.value); }
        void operator() (const YearFilter &y) { filter.add_year(y.type, y.value); }
        void operator() (const PriceFilter &p) { filter.add_price(p.type, p.value); }
    };
    auto it = s.begin();
    const auto end = s.end();
    filter_grammar gr;
    FieldFilter filter_spec;
    // string value should be fully parsed for success
    if (!(qi::parse(it, end, gr, filter_spec) && it == end))
        return false;
    FilterMaker maker(filter);
    boost::apply_visitor(maker, filter_spec);
    return true;
}


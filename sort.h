#ifndef ____SORT____
#define ____SORT____

// sorter has two implementation:
// for meaningful comparator it accumulate records,
// sort them and then output them;
// for empty comparator it just output all incoming
// records

struct NoCompare {};

// real sorting
template <class Comparator>
class Sorter
{
private:
    std::ostream &out_;
    Comparator &cmp_;
    typedef std::pair<Record, std::string> Data;
    std::vector<Data> data_;
public:
    Sorter(std::ostream &out, Comparator &cmp): out_(out), cmp_(cmp) {}
    void push(std::string &&line, Record &&rec)
    {
        data_.emplace_back(std::move(rec), std::move(line));
    }
    void flush()
    {
        std::sort(data_.begin(), data_.end(),
                [this] (const Data &lhs, const Data &rhs) {
                    return cmp_(lhs.first, rhs.first);
                });
        for (const auto &d : data_)
            out_ << d.second << std::endl;
    }
};

// short circuit sorter
template <>
class Sorter<NoCompare>
{
private:
    std::ostream &out_;
public:
    Sorter(std::ostream &out, const NoCompare&): out_(out) {}
    void push(std::string &&line, Record &&)
    {
        out_ << line << std::endl;
    }
    void flush() const {}
};


#endif


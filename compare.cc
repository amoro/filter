#include "compare.h"

#include <stdexcept>

#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

// single field sorting spec
struct FieldSort
{
    Field field;
    bool descending;
};

BOOST_FUSION_ADAPT_STRUCT(
    FieldSort,
    (Field, field)
    (bool, descending)
)

typedef std::string::const_iterator StringIterator;

using namespace boost::spirit;

template <class T> using rule = qi::rule<StringIterator, T>;

// sort grammar:
// sequence of single field specs,
// each spec include field name and direction;
// ascending is default, but could be specified explicitly as ":a";
// descending is specified as ":d"
struct sort_grammar : qi::grammar<StringIterator, std::vector<FieldSort>()>
{
    sort_grammar(): sort_grammar::base_type{values} {
        field = ("brand" >> attr(Field::brand)) |
            ("model" >> attr(Field::model)) |
            ("year" >> attr(Field::year)) |
            ("price" >> attr(Field::price));
        descending = (":d" >> attr(true)) | (":a" >> attr(false)) | attr(false);
        value = field >> descending;
        values = value % ',';
    }
    rule<Field()> field;
    rule<bool()> descending;
    rule<FieldSort()> value;
    rule<std::vector<FieldSort>()> values;
};

// case of several sortings by the same field is disallowed
void check_dups(const std::vector<FieldSort> &sort_fields)
{
    auto it = sort_fields.begin();
    const auto end = sort_fields.end();
    for (; it != end; ++it)
        if (end != std::find_if(it+1, end, [it](const FieldSort &other) { return it->field == other.field; }))
            throw std::invalid_argument("Repeated sorting by the same field is not allowed: " + std::to_string(it->field));
}

bool parse_sort_spec(const std::string &s, RecordCompare &comparator)
{
    auto it = s.begin();
    const auto end = s.end();
    sort_grammar gr;
    std::vector<FieldSort> sort_fields;
    // if not parsed full string, then failure
    if (!(qi::parse(it, end, gr, sort_fields) && it == end))
        return false;
    check_dups(sort_fields);
    for (const auto &s : sort_fields)
        comparator.add(s.field, s.descending);
    return true;
}


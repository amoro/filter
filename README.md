# README #

### Overview ###

Simple filter and/or sorter for sequence of records.
Record consists of four fields: Brand (string), Model (string), Year (integer) and Price (floating point).

Filters can be:

* by Brand (equality only)
* by Year (==, !=, <, >, <=, >=)
* by Price (<, >)

Sorting can be ascending or descending by one or several fields (like in table databases).

### Building ###

Main program needs C++11 compliant compiler and Boost libraries installed.
To build main program, run `make` command in source tree root.

There is a test data generator, written in Haskell. To build it you need GHC 7.8 or newer and cabal installed. To build it in the sandbox, run

    cabal sandbox init
    cabal install --only-dependencies
    cabal build

in `test/gen` directory.

### Usage ###

Main program has following options:

    --help    print help message
    -f [ --from ] file    read data from specified file ('-' means stdin and is used by default)
    -t [ --to ] file    write data to specified file ('-' means stdout and is used by default)
    -p [ --field-separator ] str    use str as field separator (',' by default)
    -s [ --sort ] sortspec    set sorting rules by sortspec (empty by default)
    -F [ --filter ] filterspec    add another filter by filterspec (there are none by default)

sortspec is a string consisting of one or several fields, comma separated. Each field has a format `field:x` where `field` is a name of field (`brand`, `model`, `year`, `price`) and `x` is either `a` or `d` (ascending or descending) and can be omitted (ascending is a default).
Example:

    brand    sort by brand, ascending
    year:d,price    sort by year (descending), then by price (ascending)
    model:a    sort by model, ascending

filterspec is a string, describing one filter. There are filters by brand, year and price:

    brand==value
    year<=value
    price>value

For brand value should be string and only equality (`==`) operator is allowed. For year value should be integer and all operators (`==`, `!=`, `<`, `>`, `<=`, `>=`) are allowed. For price value should be integer or floating point and only less and greater operators (`<`, `>`) are allowed.

One can specify several `-F` arguments in command line, filters will be combined.

Test generator has single command line argument - number, which, multiplied by 10, sets number of generated records.

### Author ###

Alexander Morozov
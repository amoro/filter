#include "cmd_args.h"

#include <iostream>
#include <fstream>

#include <boost/program_options.hpp>
#include <boost/program_options/errors.hpp>
namespace po = boost::program_options;

// sort specification validator:
// it should be used once in arguments,
// and it's parsed in 'compare' module
void validate(boost::any &v, const std::vector<std::string> &values, RecordCompare *target_type, int)
{
    po::validators::check_first_occurrence(v);
    const std::string &s = po::validators::get_single_string(values);
    RecordCompare rc;
    if (parse_sort_spec(s, rc))
        v = boost::any(std::move(rc));
    else
        throw po::validation_error(po::validation_error::invalid_option_value);
}

std::tuple<bool, int> parse_command_args(int argc, char **argv, Options &options)
{
    std::string from_name, to_name;
    po::variables_map var_map;
    po::options_description allowed_options("Allowed options");
    std::vector<std::string> sfilters;
    allowed_options.add_options()
        ("help", "Print help message")
        ("from,f", po::value<std::string>(&from_name)->default_value("-"), "Input file or '-' for reading from stdin")
        ("to,t", po::value<std::string>(&to_name)->default_value("-"), "Output file or '-' for writing to stdout")
        ("field-separator,p", po::value<std::string>(&options.field_separator)->default_value(","), "Field separator")
        ("sort,s", po::value<RecordCompare>(&options.comparator), "Sorting rules")
        ("filter,F", po::value<std::vector<std::string>>(&sfilters), "Filtering rules")
        ;
    try {
        po::parsed_options opts = po::command_line_parser(argc, argv).options(allowed_options).run();
        po::store(opts, var_map);
        po::notify(var_map);
        if (var_map.count("help") != 0) {
            // user asked for help, normal termination
            std::cout << allowed_options << std::endl;
            return std::make_tuple(true, 0);
        }
        // each filter is specified separately,
        // parsing them one by one
        for (const auto &s : sfilters) {
            if (!add_filter_spec(s, options.filter))
                throw std::invalid_argument("invalid filter specification - " + s);
        }
    } catch (const po::error &err) {
        // error from boost::program_options, something is wrong
        // with arguments parsing
        std::cerr << "Error in arguments: " << err.what() << std::endl;
        std::cerr << allowed_options << std::endl;
        return std::make_tuple(true, 1);
    } catch (const std::invalid_argument &err) {
        // error from our parsing facilities, some option is specified
        // incorrectly
        std::cerr << "Error in arguments: " << err.what() << std::endl;
        std::cerr << allowed_options << std::endl;
        return std::make_tuple(true, 1);
    }
    if (from_name == "-") // this means stdin
        options.input = wrap_cin();
    else
        options.input = wrap_ifstream(std::unique_ptr<std::ifstream>(new std::ifstream(from_name)));
    if (to_name == "-") // this means stdout
        options.output = wrap_cout();
    else
        options.output = wrap_ofstream(std::unique_ptr<std::ofstream>(new std::ofstream(to_name)));
    return std::make_tuple(false, 0);
}


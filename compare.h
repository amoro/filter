#ifndef ____COMPARE____
#define ____COMPARE____

#include <string>
#include <vector>
#include <memory>
#include <algorithm>

#include "record.h"

namespace compare {

// abstract comparator interface and then comparators for
// all possible sorting by one field

struct Comparator
{
    virtual bool operator() (const Record &lhs, const Record &rhs) const = 0;
};

struct BrandAsc: public Comparator
{
    bool operator() (const Record &lhs, const Record &rhs) const override
    {
        return lhs.brand < rhs.brand;
    }
};

struct BrandDsc: public Comparator
{
    bool operator() (const Record &lhs, const Record &rhs) const override
    {
        return lhs.brand > rhs.brand;
    }
};

struct ModelAsc: public Comparator
{
    bool operator() (const Record &lhs, const Record &rhs) const override
    {
        return lhs.model < rhs.model;
    }
};

struct ModelDsc: public Comparator
{
    bool operator() (const Record &lhs, const Record &rhs) const override
    {
        return lhs.model > rhs.model;
    }
};

struct YearAsc: public Comparator
{
    bool operator() (const Record &lhs, const Record &rhs) const override
    {
        return lhs.year < rhs.year;
    }
};

struct YearDsc: public Comparator
{
    bool operator() (const Record &lhs, const Record &rhs) const override
    {
        return lhs.year > rhs.year;
    }
};

struct PriceAsc: public Comparator
{
    bool operator() (const Record &lhs, const Record &rhs) const override
    {
        return lhs.price < rhs.price;
    }
};

struct PriceDsc: public Comparator
{
    bool operator() (const Record &lhs, const Record &rhs) const override
    {
        return lhs.price > rhs.price;
    }
};

}

// complex comparator:
// it includes arbitrary sequence of simple comparators;
// several comparators by the same field are disallowed
// in parsing routine
class RecordCompare
{
private:
    // we can copy this object, hence shared_ptr
    std::vector<std::shared_ptr<compare::Comparator>> comparators_;
    void add(Field field, bool descending)
    {
        using namespace compare;
        using sptr = std::shared_ptr<Comparator>;

        switch (field) {
            case Field::brand:
                comparators_.push_back(descending ? sptr(new BrandDsc()) : sptr(new BrandAsc()));
                break;
            case Field::model:
                comparators_.push_back(descending ? sptr(new ModelDsc()) : sptr(new ModelAsc()));
                break;
            case Field::year:
                comparators_.push_back(descending ? sptr(new YearDsc()) : sptr(new YearAsc()));
                break;
            case Field::price:
                comparators_.push_back(descending ? sptr(new PriceDsc()) : sptr(new PriceAsc()));
                break;
        };
    }
    friend bool parse_sort_spec(const std::string &s, RecordCompare &comparator);
public:
    bool operator() (const Record &lhs, const Record &rhs) const
    {
        // we sort by first specified field,
        // then by second and so on;
        // next comparator is used only in case
        // of equality by current comparator
        for (const auto &cmp : comparators_) {
            if ((*cmp)(lhs, rhs))
                return true;
            if ((*cmp)(rhs, lhs))
                return false;
        }
        return false;
    }
    bool empty() const { return comparators_.empty(); }
};

// parse sort spec from string value, return success flag,
// fill comparator
bool parse_sort_spec(const std::string &s, RecordCompare &comparator);

#endif


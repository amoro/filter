#include <iostream>
#include <vector>

#include <chrono>

#include "record.h"
#include "compare.h"
#include "filter.h"
#include "cmd_args.h"
#include "sort.h"
#include "lines.h"

// just copy input to output
inline void short_circuit(std::istream &in, std::ostream &out, const std::string &sep)
{
    LineProcessor<NoFilter, NoCompare> lines({}, {});
    lines.process(in, out, sep);
}

// filter input to output
inline void just_filter(std::istream &in, std::ostream &out, const std::string &sep, const RecordFilter &filter)
{
    LineProcessor<RecordFilter, NoCompare> lines(filter, {});
    lines.process(in, out, sep);
}

// sort input and send to output
inline void just_sort(std::istream &in, std::ostream &out, const std::string &sep, const RecordCompare &comparator)
{
    LineProcessor<NoFilter, RecordCompare> lines({}, comparator);
    lines.process(in, out, sep);
}

// filter input, then sort, then output
inline void filter_and_sort(std::istream &in, std::ostream &out, const std::string &sep, const RecordFilter &filter, const RecordCompare &comparator)
{
    LineProcessor<RecordFilter, RecordCompare> lines(filter, comparator);
    lines.process(in, out, sep);
}

int main(int argc, char **argv)
{
    Options opts;
    bool need_exit;
    int exit_code;
    std::tie(need_exit, exit_code) = parse_command_args(argc, argv, opts);
    if (need_exit)
        return exit_code;
    std::istream &in = opts.input->unwrap();
    std::ostream &out = opts.output->unwrap();
    auto start = std::chrono::steady_clock::now();
    if (opts.filter.empty() && opts.comparator.empty()) {
        short_circuit(in, out, opts.field_separator);
    } else if (opts.comparator.empty()) {
        just_filter(in, out, opts.field_separator, opts.filter);
    } else if (opts.filter.empty()) {
        just_sort(in, out, opts.field_separator, opts.comparator);
    } else {
        filter_and_sort(in, out, opts.field_separator, opts.filter, opts.comparator);
    }
    auto end = std::chrono::steady_clock::now();
    auto diff = end - start;
    std::cerr << std::chrono::duration<double, std::milli>(diff).count() << std::endl;
    return 0;
}


#ifndef ____RECORD____
#define ____RECORD____

#include <string>
#include <iostream>

#include "either.h"

// record with four fields;
// NULL string fields are represented by empty strings,
// NULL numberic fields are represented by zero values
struct Record
{
    std::string brand;
    std::string model;
    unsigned year;
    double price;
};

// parse record string and return either error (Left) or parse result (Right)
Either<std::string, Record> parse_record(const std::string &s, const std::string &sep);

enum class Field { brand, model, year, price };

namespace std {
    inline string to_string(Field field)
    {
        switch (field) {
            case Field::brand: return "brand";
            case Field::model: return "model";
            case Field::year: return "year";
            case Field::price: return "price";
        };
    }
}

#endif


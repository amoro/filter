#ifndef ____LINES____
#define ____LINES____

#include <iostream>
#include <string>

#include "sort.h"

// the heart of filter-sorter - 
// process input by lines, filter them if needed,
// sort them if needed, output them

// empty filer - accept every record
struct NoFilter
{
    template <class R>
    bool operator() (R &&r) const { return true; }
};

// main implementation for generic filter and comparator
template <class Filter, class Comparator>
class LineProcessor
{
private:
    Filter filter_;
    Comparator comparator_;
public:
    LineProcessor(const Filter &filter, const Comparator &comparator)
        : filter_(filter), comparator_(comparator)
    {
    }
    void process(std::istream &in, std::ostream &out, const std::string &sep)
    {
        std::string line;
        // sorter could be pass-through or real sorter,
        // so it incapsulate output strategy
        Sorter<Comparator> sorter(out, comparator_);
        auto report_error = [] (const std::string &m) { std::cerr << m << std::endl; };
        auto filter_out = [&line, &sorter, this] (Record &&rec) {
            if (filter_(rec))
                sorter.push(std::move(line), std::move(rec));
        };
        // read every line, send parsed to filter and then to sorter,
        // send unparsed to stderr
        while (std::getline(in, line))
            either(parse_record(line, sep),
                    report_error,
                    filter_out);
        sorter.flush();
    }
};

// special implementation of case when we do not
// have filters or sorters, so we can simply copy
// input to output
template <>
struct LineProcessor<NoFilter, NoCompare>
{
    LineProcessor(const NoFilter&, const NoCompare&) {}
    void process(std::istream &in, std::ostream &out, const std::string &) const
    {
        out << in.rdbuf();
    }
};


#endif


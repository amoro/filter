#ifndef ____CMD_ARGS____
#define ____CMD_ARGS____

#include <tuple>

#include "io.h"
#include "compare.h"
#include "filter.h"

struct Options
{
    std::unique_ptr<InputStream> input;
    std::unique_ptr<OutputStream> output;
    std::string field_separator;
    RecordCompare comparator;
    RecordFilter filter;
};

// parse command args, return need for exit flag and exit code
std::tuple<bool, int> parse_command_args(int argc, char **argv, Options &opts);

#endif


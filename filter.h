#ifndef ____FILTER____
#define ____FILTER____

#include <string>
#include <vector>
#include <memory>
#include <tuple>

#include <stdexcept>

#include "record.h"

namespace filter {

enum class FilterType { eq, ne, lt, lte, gt, gte };

}

namespace std {
    using filter::FilterType;
    inline string to_string(FilterType type)
    {
        switch (type) {
            case FilterType::eq: return "==";
            case FilterType::ne: return "!=";
            case FilterType::lt: return "<";
            case FilterType::lte: return "<=";
            case FilterType::gt: return ">";
            case FilterType::gte: return ">=";
        }
    }
}

namespace filter {

// filters by brand, year and price -
// could be stacked (combined by logical AND) within the same field
// by '+=' operator

// simple case of filtering by brand - only equality
// comparison, stacking is not allowed unless etalon values
// are equal
class BrandFilter
{
private:
    std::string etalon_;
public:
    explicit BrandFilter(const std::string &etalon)
        : etalon_(etalon)
    {
    }
    bool operator() (const Record &rec) const
    {
        return etalon_ == rec.brand;
    }
    BrandFilter& operator+= (const BrandFilter &other)
    {
        if (etalon_ != other.etalon_)
            throw std::invalid_argument("Filtering by brand with different values is meaningless: " +
                    etalon_ + " and " + other.etalon_);
        return *this;
    }
};

// abstract filter by comparing to some value
template <class T>
struct BasicFilter
{
    virtual bool operator() (T) const = 0;
};

// and series of basic filters for each filtering operation

template <class T>
class Eq
    : public BasicFilter<T>
{
private:
    T value_;
public:
    explicit Eq(T value): value_(value) {}
    bool operator() (T t) const override
    {
        return value_ == t;
    }
};

template <class T>
class NotEq
    : public BasicFilter<T>
{
private:
    T value_;
public:
    explicit NotEq(T value): value_(value) {}
    bool operator() (T t) const override
    {
        return value_ != t;
    }
};

template <class T>
class LessThan
    : public BasicFilter<T>
{
private:
    T value_;
public:
    explicit LessThan(T value): value_(value) {}
    bool operator() (T t) const override
    {
        return t < value_;
    }
};

template <class T>
class LessThanOrEq
    : public BasicFilter<T>
{
private:
    T value_;
public:
    explicit LessThanOrEq(T value): value_(value) {}
    bool operator() (T t) const override
    {
        return t <= value_;
    }
};

template <class T>
class GreaterThan
    : public BasicFilter<T>
{
private:
    T value_;
public:
    explicit GreaterThan(T value): value_(value) {}
    bool operator() (T t) const override
    {
        return t > value_;
    }
};

template <class T>
class GreaterThanOrEq
    : public BasicFilter<T>
{
private:
    T value_;
public:
    explicit GreaterThanOrEq(T value): value_(value) {}
    bool operator() (T t) const override
    {
        return t >= value_;
    }
};

// compile time specification of list of allowed filters types

// all types are allowed
struct AnyOp
{
    static constexpr bool check_type(FilterType type) { return true; }
};

// only some types are allowed
template <FilterType... AllowedTypes>
class ThisOps
{
private:
    static constexpr std::array<FilterType, sizeof...(AllowedTypes)> allowed_types_ = { AllowedTypes... };
public:
    static constexpr bool check_type(FilterType type)
    {
        return allowed_types_.end() != std::find(
                allowed_types_.begin(), allowed_types_.end(), type);
    }
};

// combination of basic filters for one field;
// stacking does not check for impossible combination
template <class T, class AllowedTypes, Field Name>
class RangeFilter
{
private:
    using thisClass = RangeFilter<T, AllowedTypes, Name>;
    using filter = BasicFilter<T>;
    using eq = Eq<T>;
    using ne = NotEq<T>;
    using lt = LessThan<T>;
    using lte = LessThanOrEq<T>;
    using gt = GreaterThan<T>;
    using gte = GreaterThanOrEq<T>;
    // we could copy this object, hence shared_ptr
    std::vector<std::shared_ptr<filter>> filters_;
public:
    bool empty() const { return filters_.empty(); }
    thisClass& operator+= (const thisClass &other)
    {
        filters_.insert(filters_.end(), other.filters_.begin(), other.filters_.end());
    }
protected:
    void add(FilterType type, T value)
    {
        // check filter type
        if (!AllowedTypes::check_type(type))
            throw std::invalid_argument(std::to_string(Name) + " " + std::to_string(type) +
                    " filter is not allowed");
        // and add new filter to combination
        switch (type) {
            case FilterType::eq:
                filters_.emplace_back(std::make_shared<eq>(value));
                break;
            case FilterType::ne:
                filters_.emplace_back(std::make_shared<ne>(value));
                break;
            case FilterType::lt:
                filters_.emplace_back(std::make_shared<lt>(value));
                break;
            case FilterType::lte:
                filters_.emplace_back(std::make_shared<lte>(value));
                break;
            case FilterType::gt:
                filters_.emplace_back(std::make_shared<gt>(value));
                break;
            case FilterType::gte:
                filters_.emplace_back(std::make_shared<gte>(value));
                break;
        };
    }
    bool check(T t) const
    {
        // checked value should satisfy all of stacked filters
        return std::all_of(filters_.begin(), filters_.end(),
                [t] (const std::shared_ptr<filter> &flt) {
                    return (*flt)(t);
                });
    }
};

// filter by year - any type of filters are allowed
struct YearFilter
    : public RangeFilter<unsigned, AnyOp, Field::year>
{
    YearFilter(FilterType type, unsigned value)
    {
        add(type, value);
    }
    bool operator() (const Record &rec) const { return check(rec.year); }
};

// filter by price - only 'less' and 'greater' type filters are allowed
struct PriceFilter
    : public RangeFilter<double, ThisOps<FilterType::lt, FilterType::gt>, Field::price>
{
    PriceFilter(FilterType type, double value) { add(type, value); }
    bool operator() (const Record &rec) const { return check(rec.price); }
};

}

// full filter specification -
// zero or one stack for each of filterable fields
class RecordFilter
{
private:
    // we could copy it, hence shared_ptr
    std::shared_ptr<filter::BrandFilter> brand_;
    std::shared_ptr<filter::YearFilter> year_;
    std::shared_ptr<filter::PriceFilter> price_;
    void add_brand(const std::string &s)
    {
        if (!brand_)
            brand_.reset(new filter::BrandFilter(s));
        else
            (*brand_) += filter::BrandFilter(s);
    }
    void add_year(filter::FilterType type, unsigned value)
    {
        if (!year_)
            year_.reset(new filter::YearFilter(type, value));
        else
            (*year_) += filter::YearFilter(type, value);
    }
    void add_price(filter::FilterType type, double value)
    {
        if (!price_)
            price_.reset(new filter::PriceFilter(type, value));
        else
            (*price_) += filter::PriceFilter(type, value);
    }
    friend bool add_filter_spec(const std::string &s, RecordFilter &filter);
public:
    bool operator() (const Record &rec) const
    {
        return (!brand_ || (*brand_)(rec)) &&
            (!year_ || (*year_)(rec)) &&
            (!price_ || (*price_)(rec));
    }
    bool empty() const { return !brand_ && !year_ && !price_; }
};

// parse one filter spec, return success flag, fill submitted filter value
bool add_filter_spec(const std::string &s, RecordFilter &filter);

#endif


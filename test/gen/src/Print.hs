module Print
  (
    hPrintRecords
  )
  where

import System.IO (Handle)
import Data.ByteString.Builder (Builder, char7, hPutBuilder, intDec, string7)
import Data.Monoid
import Data.Scientific (fromFloatDigits)
import Data.ByteString.Builder.Scientific

import Data

hPrintRecords :: Handle -> [Record] -> IO ()
hPrintRecords h rs = hPutBuilder h $ printLines printRecord rs

printLines :: (a -> Builder) -> [a] -> Builder
printLines _ []     = mempty
printLines f (x:xs) = f x <> mconcat [ sep <> f x' | x' <- xs ]
  where
    sep = char7 '\n'

maybePrint :: (a -> Builder) -> Maybe a -> Builder
maybePrint = maybe mempty

printRecord :: Record -> Builder
printRecord r = brand <> sep <> model <> sep <> year <> sep <> price
  where
    sep = char7 ','
    brand = maybePrint string7 $ getBrand r
    model = maybePrint string7 $ getModel r
    year = maybePrint intDec $ getYear r
    price = maybePrint printFloat $ getPrice r

printFloat :: Float -> Builder
printFloat = formatScientificBuilder Fixed (Just 3) . fromFloatDigits


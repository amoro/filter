module Main where

import Test.QuickCheck
import System.IO (stdout)
import System.Environment (getArgs)

import Data
import Print

main :: IO ()
main = do
  (pn:_) <- getArgs
  let n = read pn :: Int
  xs <- sample' $ generator n
  hPrintRecords stdout $ concat xs

module Data
  (
    Record
  , generator
  , getBrand
  , getModel
  , getYear
  , getPrice
  )
  where

import Control.Applicative
import Test.QuickCheck

newtype Brand = Brand String
  deriving (Show, Eq)

instance Arbitrary Brand where
  arbitrary = Brand <$> elements
    [
      "Audi"
    , "BMW"
    , "Citroen"
    , "Fiat"
    , "Honda"
    , "Huynday"
    , "Kia"
    , "Lada"
    , "Mercedes-Benz"
    , "Mazda"
    , "Mitsubishi"
    , "Pegot"
    , "Reno"
    , "Seat"
    , "Ssang-Yong"
    , "Subaru"
    , "Suzuki"
    , "Volkswagen"
    , "Volvo"
    ]

newtype Model = Model String
  deriving (Show, Eq)

instance Arbitrary Model where
  arbitrary = Model <$> (choose (2, 15) >>= (flip vectorOf letter))
    where
      letter = frequency
        [
          (14, smallLetter)
        , (3, digit)
        , (2, capitalLetter)
        , (1, otherChars)
        ]
      smallLetter = elements ['a'..'z']
      capitalLetter = elements ['A'..'Z']
      digit = elements ['0'..'9']
      otherChars = elements " -"

newtype Year = Year Int
  deriving (Show, Eq)

instance Arbitrary Year where
  arbitrary = Year <$> choose (1980, 2016)

newtype Price = Price Float
  deriving (Show, Eq)

instance Arbitrary Price where
  arbitrary = Price <$> choose (1000.0, 100000.0)

data Record = Record {
      rBrand :: Maybe Brand
    , rModel :: Maybe Model
    , rYear :: Maybe Year
    , rPrice :: Maybe Price
  }
  deriving (Show, Eq)

instance Arbitrary Record where
  arbitrary = Record <$> arbitrary <*> arbitrary <*> arbitrary <*> arbitrary

generator :: Int -> Gen [Record]
generator n = vectorOf n arbitrary

getBrand :: Record -> Maybe String
getBrand r =
  do
    Brand b <- rBrand r
    return $! b

getModel :: Record -> Maybe String
getModel r =
  do
    Model m <- rModel r
    return $! m

getYear :: Record -> Maybe Int
getYear r =
  do
    Year y <- rYear r
    return $! y

getPrice :: Record -> Maybe Float
getPrice r =
  do
    Price p <- rPrice r
    return $! p


#ifndef ____EITHER____
#define ____EITHER____

#include <boost/variant.hpp>

// the Either type represents two possibilities:
// either Left or Right;
// usually Left means error, incorrect
// and Right means meaningful value, correct
//
// here is just some sugar around Boost.Variant

template <class Left, class Right>
class Either
{
private:
    boost::variant<Left, Right> data_;
    template <class T>
    Either(T &&t): data_(std::forward<T>(t)) {}
public:
    typedef Left left_type;
    typedef Right right_type;

    template <class L, class R>
    friend Either<L, R> make_left(L &&l) { return Either<L, R>(std::move(l)); }
    template <class L, class R>
    friend Either<L, R> make_left(const L &l) { return Either<L, R>(l); }
    template <class L, class R>
    friend Either<L, R> make_right(R &&r) { return Either<L, R>(std::move(r)); }
    template <class L, class R>
    friend Either<L, R> make_right(const R &r) { return Either<L, R>(r); }
    template <class E, class L, class R>
    friend void either(E &&e, L l, R r);
    template <class E, class L, class R>
    friend void either(const E &e, L l, R r);
};

// do some action if has Left value or
// do some other action if has Right value

template <class E, class L, class R>
inline void either(E &&e, L l, R r)
{
    struct visitor: public boost::static_visitor<>
    {
        L &l;
        R &r;
        visitor(L &l_, R &r_): l(l_), r(r_) {}
        void operator() (typename std::decay<E>::type::left_type &x) const
        {
            l(std::move(x));
        }
        void operator() (typename std::decay<E>::type::right_type &x) const
        {
            r(std::move(x));
        }
    };
    boost::apply_visitor(visitor(l, r), e.data_);
}

template <class E, class L, class R>
inline void either(const E &e, L l, R r)
{
    struct visitor: public boost::static_visitor<>
    {
        L &l;
        R &r;
        visitor(L &l_, R &r_): l(l_), r(r_) {}
        void operator() (const typename std::decay<E>::type::left_type &x) const
        {
            l(x);
        }
        void operator() (const typename std::decay<E>::type::right_type &x) const
        {
            r(x);
        }
    };
    boost::apply_visitor(visitor(l, r), e.data_);
}

#endif


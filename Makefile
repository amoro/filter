CXXFLAGS=-O3 -g --std=c++11
LDFLAGS=
LIBS=-lboost_program_options
RM=rm -f
CXX=g++

SRCS=cmd_args.cc main.cc record.cc filter.cc compare.cc
OBJS=$(subst .cc,.o,$(SRCS))

all: filter

filter: $(OBJS)
	$(CXX) $(LDFLAGS) -o filter $(OBJS) $(LIBS)

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CXXFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS) || true

dist-clean: clean
	$(RM) *~ .depend

include .depend
